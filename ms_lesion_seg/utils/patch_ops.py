import numpy as np

def safe_slice(st, en, p, dim):
    '''
    Avoid out-of-bounds
    '''
    if st < 0:
        st += p
        en += p
    elif en >= dim:
        st -= p
        en -= p

    return slice(st, en)

def get_patch(img_vol, lbl_vol, patch_size, target_idx):

    # Get random rotation and center
    sts = [l - int(np.floor(p/2))
           for l, p in zip(target_idx, patch_size)]
    ens = [st + p for st, p in zip(sts, patch_size)]
    idx = [safe_slice(st, en, int(np.floor(p/2)), dim) 
           for st, en, p, dim in zip(sts, ens, patch_size, lbl_vol[0].shape)]
    
    # to index into the multi-channel volume, we ignore the channels
    patch_idx = tuple([slice(None, None)] + idx)
            
    x = img_vol[patch_idx].squeeze(-3).squeeze(-2).squeeze(-1)
    y = lbl_vol[patch_idx].squeeze(-3).squeeze(-2).squeeze(-1)
    
    return x, y
            
