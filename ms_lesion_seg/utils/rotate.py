import numpy as np
from scipy import ndimage
from tqdm import tqdm
import torch
import torch.nn.functional as F

def deg_to_rad(deg):
    return deg/180*np.pi

def rotate_vol_2D(vol, angle):
    '''
    Rotates each 2D slice in a volume by an angle
    
    Inputs:
        vol: nparray of shape (H, W, D, C)
        angle: int, angle to rotate
    Outputs:
        Rotated volume of shape (H, W, D, C)
    '''
    # all torch operations are more efficient if
    # (H, W, D, C) -> (C, D, H, W)
    vol_T = vol.transpose(3, 2, 0, 1)
    
    # Avoid interpolation errors when able to rot90
    if angle == 0 or angle == 360:
        return vol
    elif angle == 90:
        return torch.rot90(torch.from_numpy(vol_T), k=1, dims=[2, 3])\
                    .numpy()\
                    .transpose(2, 3, 1, 0)
    elif angle == 180:
        return torch.rot90(torch.from_numpy(vol_T), k=2, dims=[2, 3])\
                    .numpy()\
                    .transpose(2, 3, 1, 0)
    elif angle == 270:
        return torch.rot90(torch.from_numpy(vol_T), k=3, dims=[2, 3])\
                    .numpy()\
                    .transpose(2, 3, 1, 0)
    else:
        
        a = deg_to_rad(angle)
        
        rot_x = _calc_rotation_x(0)
        rot_y = _calc_rotation_y(0)
        rot_z = _calc_rotation_z(a)

        affine = rot_z @ rot_y @ rot_x
        affine = affine[:3, ...].astype(np.float32)
        affine = affine[np.newaxis, ...]
                
        c, d, h, w = vol_T.shape
        
        out = np.zeros_like(vol_T, dtype=np.float32)
        
        for c_idx in range(c):
            st = c_idx
            en = st + 1
            
            # (cur_channel-batch, 1, D, H, W)
            in_vol = vol_T[st:en, np.newaxis, ...]

            affine_grid = F.affine_grid(
                theta=torch.from_numpy(affine),
                align_corners=True,
                size=in_vol.shape,
            )

            out[st:en] = F.grid_sample(
                torch.from_numpy(in_vol).cuda(), 
                affine_grid.cuda(), 
                mode='bilinear', 
                padding_mode='reflection', 
                align_corners=True,
            ).detach().cpu().numpy()[:, 0, ...]
            
        # (C, D, H, W) -> (H, W, D, C)
        out = out.transpose(2, 3, 1, 0)
            
        return out
    
def _calc_rotation_x(angle):
    """Calculates 3D rotation matrix around the first axis.

    Args:
        angle (float): The rotation angle in rad.
    
    Returns:
        numpy.ndarray: The 3x3 rotation matrix.

    """
    rotation = np.array([[1, 0, 0, 0],
                         [0, np.cos(angle), -np.sin(angle), 0],
                         [0, np.sin(angle), np.cos(angle), 0],
                         [0, 0, 0, 1]])
    return rotation


def _calc_rotation_y(angle):
    """Calculates 3D rotation matrix around the second axis.

    Args:
        angle (float): The rotation angle in rad.
    
    Returns:
        numpy.ndarray: The 3x3 rotation matrix.

    """
    rotation = np.array([[np.cos(angle), 0, np.sin(angle), 0],
                         [0, 1, 0, 0],
                         [-np.sin(angle), 0, np.cos(angle), 0],
                         [0, 0, 0, 1]])
    return rotation


def _calc_rotation_z(angle):
    """Calculates 3D rotation matrix around the third axis.

    Args:
        angle (float): The rotation angle in rad.
    
    Returns:
        numpy.ndarray: The 3x3 rotation matrix.

    """
    rotation = np.array([[np.cos(angle), -np.sin(angle), 0, 0],
                         [np.sin(angle), np.cos(angle), 0, 0],
                         [0, 0, 1, 0],
                         [0, 0, 0, 1]])
    return rotation
