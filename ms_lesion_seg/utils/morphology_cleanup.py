from skimage import morphology
from skimage import measure
import numpy as np
from .membership import *

def get_largest_connected_component(lbl_vol):
    '''
    input: integer-labeled label volume
    output: dict: label to smallest # connected components for that label
    
    acronym: cc = connected component
    '''
    # convert to membership
    label_map = index_labels(lbl_vol)
    lbl_vol = apply_relabel(lbl_vol, label_map)
    mem = mask_to_membership(lbl_vol, len(np.unique(lbl_vol)))
    
    inv_label_map = {v:k for k, v in label_map.items()}
    
    largest_ccs = {}
    
    for c in range(mem.shape[-1]):
#         if c == 0:
#             continue
        components = measure.label(mem[..., c], connectivity=3)
        props = measure.regionprops(components)
        areas = [x.area for x in props]
        largest_ccs[inv_label_map[c]] = np.max(areas)
        
    return largest_ccs

def get_cc_vol(ccs, voxel_vol):
    '''
    inputs:
        ccs: list of largest/smallest ccs
        voxel_vol: real-world volume of voxel
    outputs: dict: label to real world volume
    '''
    return {k:voxel_vol*v for k, v in ccs.items()}

def get_target_voxcount(src_cc_vols, dst_voxel_vol):
    '''
    inputs:
        src_cc_vols: Dict of label -> real world volume for LR volume
        dst_voxel_vol: real-world volume of voxel estimate for HR volume
    outputs:
        dict of label -> real world volume estimate for HR volume
        
    This function generally will overestimate # voxels compared to GT.
    '''
    return {k:int(v/dst_voxel_vol) for k, v in src_cc_vols.items()}
    

def cleanup(lbl_vol, voxel_counts):
    '''
    input: 
        lbl_vol: integer-labeled label volume
        voxel_counts: dict: label -> estimated largest connected component for that label
    output: integer-labeled label volume
    '''
        
    # convert to membership
    label_map = index_labels(lbl_vol)
    lbl_vol = apply_relabel(lbl_vol, label_map)
    mem = mask_to_membership(lbl_vol, len(np.unique(lbl_vol)))
    
    # remap voxel_count to ascending labels
    voxel_counts = {label_map[k]:v for k, v in voxel_counts.items()}
    
    out_vol = np.zeros_like(mem, dtype=np.float32)
    # loop over binary channels and clean up with
    # binary morphology
    for c in range(mem.shape[-1]):
        if c == 0:
            continue
        # remove objects
        tmp = morphology.remove_small_objects(
            mem[..., c].astype(bool), 
            min_size=voxel_counts[c], 
            connectivity=len(mem.shape),
        )
        # place
        out_vol[..., c] = tmp
    
    # convert to label vol
    out_vol = membership_to_mask(out_vol)
    
    # relabel
    out_vol = apply_relabel(out_vol, {v:k for k,v in label_map.items()})
    # return
    return out_vol