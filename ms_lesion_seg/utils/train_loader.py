import numpy as np
import torch
from torch.utils.data import Dataset
from .patch_ops import get_patch
import nibabel as nib
from tqdm import tqdm


def load_imgs(fpath_list):
    fl_mri_fpath = [x for x in fpath_list if 'FLAIR' in x.name][0]
    pd_mri_fpath = [x for x in fpath_list if 'PD' in x.name][0]
    t1_mri_fpath = [x for x in fpath_list if 'MPRAGE' in x.name][0]
    t2_mri_fpath = [x for x in fpath_list if 'T2' in x.name][0]

    label_fpath = [x for x in fpath_list if 'mask' in x.name][0]

    lbl = nib.load(label_fpath).get_fdata(dtype=np.float32)
    img = np.zeros((4, *lbl.shape), dtype=np.float32)

    img[0] = nib.load(fl_mri_fpath).get_fdata(dtype=np.float32)
    img[1] = nib.load(pd_mri_fpath).get_fdata(dtype=np.float32)
    img[2] = nib.load(t1_mri_fpath).get_fdata(dtype=np.float32)
    img[3] = nib.load(t2_mri_fpath).get_fdata(dtype=np.float32)

    return torch.from_numpy(img), torch.from_numpy(lbl).unsqueeze(0)


def normalize(x, a=-1, b=1):
    '''
    scale within [a, b], but also assume we can predict up to
    110% of the maximum intensity of x

    This allows the model some leeway in prediction.
    '''

    # pass through all-zero slices
    if x.max() == 0 and x.min() == 0:
        return x, 0, 0

    lower = x.min()

    upper = x.max()

    y = a + ((x - lower)*(b-a)) / (upper - lower)

    return y


def flip_xy(a, b):
    # Here, we expect `a` to dedicate its first
    # axis to channels. Therefore we must skip it
    if np.random.random() < 0.5:
        a = torch.flip(a, dims=[1])
        b = torch.flip(b, dims=[1])
    if np.random.random() < 0.5:
        a = torch.flip(a, dims=[2])
        b = torch.flip(b, dims=[2])
    return a, b


class TrainLoader(Dataset):
    def __init__(
        self,
        data_dir,
        patch_size,
        drop_idx,
        n_steps,
        dtype=np.float32,
        a=0,
        b=1,
    ):
        self.patch_size = patch_size
        self.imgs = []
        self.lbls = []
        self.lesion_idxs = []
        self.healthy_idxs = []
        self.lesion_choices = []
        self.healthy_choices = []
        self.n_steps = n_steps

        ##### Decide patient ids #####
        # hard-coded for this experiment since we know which patients we want
        patient_ids = [f"{x:03d}" for x in [1, 2, 3, 7, 8, 9, 10, 11, 12]]
        if drop_idx != -1:
            val_id = patient_ids.pop(drop_idx)
            val_fpath_list = [
                x for x in data_dir.iterdir() if val_id in x.name]
            ##### Load validation into RAM #####
            self.val_img, self.val_lbl = load_imgs(val_fpath_list)
            self.val_img = normalize(self.val_img, a=a, b=b)
        else:
            self.val_img = None
            self.val_lbl = None

        # organize remaining fpaths
        patient_dict = {
            patient_id: [x for x in data_dir.iterdir() if patient_id in x.name]
            for patient_id in patient_ids
        }

        print('='*20, 'Loading images into RAM', '='*20)
        ##### Load volumes into RAM #####
        for fpath_list in tqdm(patient_dict.values()):
            img, lbl = load_imgs(fpath_list)

            # normalize images
            # this step is simply to ease training;
            # we assume all data is in DeepHarmony space
            img = normalize(img, a=a, b=b)

            self.imgs.append(img)
            self.lbls.append(lbl)
            self.lesion_idxs.append(np.where(lbl[0] == 1))
            self.healthy_idxs.append(np.where(lbl[0] == 0))

            # track possible lesion and healthy index choices
            # by the most recently appended index list
            self.lesion_choices.append(np.arange(len(self.lesion_idxs[-1][0])))
            self.healthy_choices.append(
                np.arange(len(self.healthy_idxs[-1][0])))

            # shuffle possible choices by most recently appended index list
            np.random.shuffle(self.lesion_choices[-1])
            np.random.shuffle(self.healthy_choices[-1])

    def __len__(self):
        return self.n_steps

    def __getitem__(self, i):
        idx = i % len(self.imgs)

        # choose a random orientation
        patch_perm = np.random.permutation(self.patch_size)

        if np.random.random() < 0.25:
            choice_idx = i % len(self.lesion_choices[idx])
            a, b, c = self.lesion_idxs[idx]
        else:
            choice_idx = i % len(self.healthy_choices[idx])
            a, b, c = self.healthy_idxs[idx]

        target_idx = (a[choice_idx].item(),
                      b[choice_idx].item(), c[choice_idx].item())

        x, y = get_patch(
            self.imgs[idx], self.lbls[idx], patch_perm, target_idx)

        x, y = flip_xy(x, y)
        return x, y
