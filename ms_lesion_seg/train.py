from pathlib import Path
import numpy as np
import nibabel as nib
import os
import sys
from utils.train_loader import TrainLoader
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter
from models.cnn import CNN
from models.cnn_pools import CNNPools
from models.unet import UNet
import torch
from tqdm import tqdm
import gc
from datetime import datetime
import time
torch.backends.cudnn.benchmark = True


# Jacob's
def dice_loss(input, target):
    # this loss function need input in the range (0, 1), and target in (0, 1)
    smooth = 0.01

    iflat = input.view(-1)
    tflat = target.view(-1)
    intersection = (iflat * tflat).sum()

    return 1 - ((2. * intersection + smooth) / (iflat.sum() + tflat.sum() + smooth))


class DiceLoss(torch.nn.Module):
    def __init__(self):
        super(DiceLoss, self).__init__()
        self.smooth = 1

    def forward(self, input, target):
        axes = tuple(range(1, input.dim()))
        intersect = (input * target).sum(dim=axes)
        union = torch.pow(input, 2).sum(dim=axes) + \
            torch.pow(target, 2).sum(dim=axes)
        loss = 1 - (2 * intersect + self.smooth) / (union + self.smooth)
        return loss.mean()


class FocalLoss(torch.nn.Module):
    def __init__(self, gamma=2):
        super(FocalLoss, self).__init__()
        self.gamma = gamma
        self.eps = 1e-3

    def forward(self, input, target):
        input = input.clamp(self.eps, 1 - self.eps)
        loss = - (target * torch.pow((1 - input), self.gamma) * torch.log(input) +
                  (1 - target) * torch.pow(input, self.gamma) * torch.log(1 - input))
        return loss.mean()


def validate(val_img, val_lbl, model):
    pred_vol_1 = torch.zeros_like(val_lbl)
    pred_vol_2 = torch.zeros_like(val_lbl)
    pred_vol_3 = torch.zeros_like(val_lbl)
    print("Validating...")

    for i in tqdm(range(val_img.shape[1])):
        x_val = val_img[:, i, ...].unsqueeze(0)
        y_val = val_lbl[:, i, ...].unsqueeze(0)

        pred = model(x_val.to(device))
        pred_vol_1[0, i, ...] = pred[:, 0, ...].detach().cpu()

    for i in tqdm(range(ds.val_img.shape[2])):
        x_val = val_img[:, :, i, ...].unsqueeze(0)
        y_val = val_lbl[:, :, i, ...].unsqueeze(0)

        pred = model(x_val.to(device))
        pred_vol_2[0, :, i, ...] = pred[:, 0, ...].detach().cpu()

    for i in tqdm(range(ds.val_img.shape[3])):
        x_val = val_img[:, :, :, i, ...].unsqueeze(0)
        y_val = val_lbl[:, :, :, i, ...].unsqueeze(0)

        pred = model(x_val.to(device))
        pred_vol_3[0, :, :, i, ...] = pred[:, 0, ...].detach().cpu()

    pred_vol_1 = torch.sigmoid(pred_vol_1)
    pred_vol_2 = torch.sigmoid(pred_vol_2)
    pred_vol_3 = torch.sigmoid(pred_vol_3)

    pred_vol = torch.mean(torch.stack(
        [pred_vol_1, pred_vol_2, pred_vol_3]), dim=0)

    pred_vol[pred_vol > 0.5] = 1
    pred_vol[pred_vol != 1] = 0

    return pred_vol


if __name__ == '__main__':

    train_st = time.time()

    data_dir = Path("/iacl/pg20/sam/data/ms_lesions_2021/validation")

    patch_size = (64, 64, 1)
    batch_size = 32
    n_steps = 2000000
    lr = 5e-5
    loss_weight = 0.8
    val_freq = 3000
    gpu_id = 0
    drop_idx = int(sys.argv[1])
    device = torch.device(f'cuda:{gpu_id}')
    model_name = "CNN"

    now = datetime.now()
    today = now.strftime("%Y%m%d")
    exp_start_time = now.strftime("%H%M%S")

    experiment = f"{exp_start_time}_{model_name}_focalloss_nsteps={n_steps}_dropidx={drop_idx}_lossweight={loss_weight}_ps={patch_size[0]}_bs={batch_size}"
    writer = SummaryWriter(log_dir=Path(f"./tb/{today}/{experiment}"))

    ds = TrainLoader(
        data_dir=data_dir,
        patch_size=patch_size,
        drop_idx=drop_idx,
        n_steps=n_steps,
    )

    data_loader = DataLoader(
        ds,
        batch_size=batch_size,
        shuffle=False,  # random patches are drawn
        pin_memory=True,
        num_workers=8,
    )

    if model_name == 'CNN':
        model = CNN(
            n_img_channels=4,
            n_lbl_classes=1,
            kernel_size=3,
            n_layers=6,
            filters=256,
        ).to(device)
    elif model_name == "unet":
        model = UNet(
            n_channels=4,
            n_classes=1,
        ).to(device)

    opt = torch.optim.AdamW(model.parameters(), lr=lr)
    scheduler = torch.optim.lr_scheduler.OneCycleLR(
        optimizer=opt,
        max_lr=lr,
        total_steps=n_steps,
        cycle_momentum=True,
    )

    scaler = torch.cuda.amp.GradScaler()
    dice_loss_obj = DiceLoss()
    focal_loss_obj = FocalLoss(2)

    global_step = 0

    print("="*20, "Training...", "="*20)
    with tqdm(total=n_steps) as pbar:
        for i, (x, y) in enumerate(data_loader):
            opt.zero_grad()
            x_device = x.to(device)
            y_device = y.to(device)

            with torch.cuda.amp.autocast():
                y_hat_logits = model(x_device)
                y_hat = torch.sigmoid(y_hat_logits)
                dice = dice_loss_obj(y_hat, y_device)
                loss = loss_weight * dice + \
                    (1 - loss_weight) * focal_loss_obj(y_hat_logits, y_device)

                scaler.scale(loss).backward()
                scaler.step(opt)

                prev_scale = scaler.get_scale()
                scaler.update()
                # prevent scheduler update when gradients overflow
                if prev_scale == scaler.get_scale():
                    scheduler.step()

            writer.add_scalar(
                "Loss",
                loss.detach().cpu().numpy(),
                global_step,
            )
            writer.add_scalar(
                "Dice",
                1 - dice.detach().cpu().numpy(),
                global_step,
            )

            if drop_idx != -1 and (i + 1) % val_freq == 0:
                pred_vol = validate(ds.val_img, ds.val_lbl, model)
                val_dice = 1 - \
                    dice_loss_obj(pred_vol.flatten(), ds.val_lbl.flatten())
                writer.add_scalar(
                    "Val Dice",
                    val_dice.detach().cpu().numpy(),
                    global_step,
                )

            global_step += 1

            pbar.set_postfix(
                {
                    'loss': f'{loss.detach().cpu().numpy():.4f}',
                    'dice': f'{1 - dice.detach().cpu().numpy():.4f}',
                }
            )
            pbar.update(batch_size)
    weight_path = Path("./weights") / \
        f"{today}_{exp_start_time}_{model_name}_dropidx={drop_idx}_best_weights.h5"
    print()
    torch.save({'model': model.state_dict()}, str(weight_path))

    train_en = time.time()

    print("="*20, "END TRAINING", "="*20)
    print(f"\tElapsed time: {train_en - train_st:.4f}s")
