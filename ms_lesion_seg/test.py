from pathlib import Path
import argparse
import numpy as np
import nibabel as nib
import os
import sys
from utils.train_loader import TrainLoader, normalize
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter
from models.cnn import CNN
from models.cnn_pools import CNNPools
from models.unet import UNet
import torch
from tqdm import tqdm
import gc
from datetime import datetime
import time
torch.backends.cudnn.benchmark = True


def segment(inp, model):
    pred_vol_1 = torch.zeros_like(inp[0])
    pred_vol_2 = torch.zeros_like(inp[0])
    pred_vol_3 = torch.zeros_like(inp[0])

    for i in range(inp.shape[1]):
        x_val = inp[:, i, ...].unsqueeze(0)
        pred = model(x_val.to(device))
        pred_vol_1[i, ...] = pred[:, 0, ...].detach().cpu()

    for i in range(inp.shape[2]):
        x_val = inp[:, :, i, ...].unsqueeze(0)
        pred = model(x_val.to(device))
        pred_vol_2[:, i, ...] = pred[:, 0, ...].detach().cpu()

    for i in range(inp.shape[3]):
        x_val = inp[:, :, :, i, ...].unsqueeze(0)
        pred = model(x_val.to(device))
        pred_vol_3[:, :, i, ...] = pred[:, 0, ...].detach().cpu()

    pred_vol_1 = torch.sigmoid(pred_vol_1)
    pred_vol_2 = torch.sigmoid(pred_vol_2)
    pred_vol_3 = torch.sigmoid(pred_vol_3)

    pred_vol = torch.mean(torch.stack(
        [pred_vol_1, pred_vol_2, pred_vol_3]), dim=0)

    pred_vol[pred_vol > 0.5] = 1
    pred_vol[pred_vol != 1] = 0

    return pred_vol


if __name__ == '__main__':
    args = None
    parser = argparse.ArgumentParser()
    parser.add_argument('--fl-fpath', type=str, required=True)
    parser.add_argument('--pd-fpath', type=str, required=True)
    parser.add_argument('--t1-fpath', type=str, required=True)
    parser.add_argument('--t2-fpath', type=str, required=True)
    parser.add_argument('--out-dir', type=str, required=True)
    parser.add_argument('--gpu-id', type=int, default=0)
    parser.add_argument('--weights-dir', type=str, required=True)
    args = parser.parse_args(args if args is not None else sys.argv[1:])

    now = datetime.now()
    today = now.strftime("%Y%m%d")
    exp_start_time = now.strftime("%H%M%S")

    device = torch.device(f'cuda:{args.gpu_id}')
    model_name = "CNN"

    inp_obj = nib.load(args.fl_fpath)
    affine = inp_obj.affine

    final_out = np.zeros_like(inp_obj.get_fdata(dtype=np.float32), dtype=np.float32)

    for i, weight_fpath in enumerate(tqdm(sorted(Path(args.weights_dir).iterdir()))):
        checkpoint = torch.load(weight_fpath)

        model = CNN(
            n_img_channels=4,
            n_lbl_classes=1,
            kernel_size=3,
            n_layers=2,
            filters=256,
        )

        model.load_state_dict(checkpoint['model'])
        model.to(device).eval()

        affine = nib.load(args.fl_fpath).affine

        fl = nib.load(args.fl_fpath).get_fdata(dtype=np.float32)
        pd = nib.load(args.pd_fpath).get_fdata(dtype=np.float32)
        t1 = nib.load(args.t1_fpath).get_fdata(dtype=np.float32)
        t2 = nib.load(args.t2_fpath).get_fdata(dtype=np.float32)

        inp_img = np.zeros((4, *fl.shape), dtype=np.float32)
        inp_img[0] = fl
        inp_img[1] = pd
        inp_img[2] = t1
        inp_img[3] = t2
        inp_img = torch.from_numpy(inp_img)
        inp_img = normalize(inp_img, a=0, b=1)

        pred_vol = segment(inp_img, model).detach().cpu().numpy()

        final_out += pred_vol

        out_fpath = Path(args.out_dir) / \
            (Path(args.t1_fpath).name.split('.nii')[0] + f'_seg{i}.nii.gz')

        obj = nib.Nifti1Image(pred_vol, affine=affine)
        nib.save(obj, str(out_fpath))

    final_out /= 10
    final_out[final_out >= 0.5] = 1
    final_out[final_out != 1] = 0
    out_fpath = Path(args.out_dir) / \
        (Path(args.t1_fpath).name.split('.nii')[0] + '_seg_final.nii.gz')
    obj = nib.Nifti1Image(pred_vol, affine=affine)
    nib.save(obj, str(out_fpath))
