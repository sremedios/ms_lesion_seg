import os
import sys
from pathlib import Path

root_dir = Path("/iacl/pg20/sam/data/ms_lesions_2021/validation")

gpu_id = 0
weights_dir = Path("/home/sam/projects/ms_lesion_seg/ms_lesion_seg/weights/final_weights")
out_dir = Path("/iacl/pg20/sam/data/ms_lesions_2021/preds")

fpaths = list(root_dir.iterdir())

fls = sorted([x for x in fpaths if "FLAIR" in x.name])
pds = sorted([x for x in fpaths if "PD" in x.name])
t1s = sorted([x for x in fpaths if "MPRAGE" in x.name])
t2s = sorted([x for x in fpaths if "T2" in x.name])

for i in range(len(fls)):
    fl = fls[i]
    pd = pds[i]
    t1 = t1s[i]
    t2 = t2s[i]

    cmd = f"python test.py --fl-fpath {fl} --pd-fpath {pd} --t1-fpath {t1} --t2-fpath {t2} --out-dir {out_dir} --gpu-id {gpu_id} --weights-dir {weights_dir}"

    os.system(cmd)
