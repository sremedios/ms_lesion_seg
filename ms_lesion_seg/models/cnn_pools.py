import torch
import torch.nn as nn
import torch.nn.functional as F


class CALayer(nn.Module):
    def __init__(self, filters, reduction=16):
        super(CALayer, self).__init__()

        # global average pooling: feature --> point
        self.avg_pool = nn.AdaptiveAvgPool2d(1)

        # feature channel downscale and upscale --> channel weight
        self.conv_du = nn.Sequential(
            nn.Conv2d(filters, filters // reduction, 1, padding=0, bias=True),
            nn.ReLU(inplace=True),
            nn.Conv2d(filters // reduction, filters, 1, padding=0, bias=True),
            nn.Sigmoid()
        )

    def forward(self, x):
        y = self.avg_pool(x)
        y = self.conv_du(y)
        return x * y


class ConvReLU(nn.Module):
    def __init__(
        self,
        in_channels,
        out_channels,
        kernel_size,
        padding_mode='zeros',
    ):
        super(ConvReLU, self).__init__()

        padding = kernel_size // 2

        self.body = nn.Sequential(
            nn.Conv2d(
                in_channels=in_channels,
                out_channels=out_channels,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
            nn.ReLU(),
        )

    def forward(self, x):
        return self.body(x)


class CNNPools(nn.Module):
    def __init__(
        self,
        shape,
        n_img_channels=4,
        n_lbl_classes=1,
        kernel_size=3,
        n_layers=32,
        filters=256,
        padding_mode='zeros',
    ):
        super(CNNPools, self).__init__()
        padding = kernel_size // 2

        self.filters = filters
        self.n_layers = n_layers

        # initial convolution
        self.head = ConvReLU(
            n_img_channels,
            filters,
            kernel_size,
            padding_mode,
        )

        self.body = nn.Sequential(
            ConvReLU(
                filters,
                filters,
                kernel_size,
                padding_mode,
            ),
            nn.MaxPool2d(kernel_size=2),
            ConvReLU(
                filters,
                filters,
                kernel_size,
                padding_mode,
            ),
            nn.MaxPool2d(kernel_size=2),
            ConvReLU(
                filters,
                filters,
                kernel_size,
                padding_mode,
            ),
            nn.UpsamplingBilinear2d(scale_factor=2),
            ConvReLU(
                filters,
                filters,
                kernel_size,
                padding_mode,
            ),
            nn.UpsamplingBilinear2d(size=shape),
            ConvReLU(
                filters,
                filters,
                kernel_size,
                padding_mode,
            ),
        )

        self.attn = CALayer(filters=filters, reduction=1)

        self.tail = nn.Sequential(
            nn.Conv2d(
                in_channels=filters,
                out_channels=n_lbl_classes,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
        )

    def forward(self, img):
        head = self.head(img)
        body = self.body(head)
        attn = self.attn(head + body)
        tail = self.tail(attn)

        return tail
